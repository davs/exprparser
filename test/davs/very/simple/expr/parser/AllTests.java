package davs.very.simple.expr.parser;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
	{ LexerTest.class, ParserTest.class, ExpressionsTest.class })
public class AllTests
	{

	}
