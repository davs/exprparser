package davs.very.simple.expr.parser;

import static org.junit.Assert.*;

import org.junit.Test;

public class LexerTest
	{
		public static int LPAREN = 2;
		public static int RPAREN = 3;
		public static int NUMBER = 4;
		public static int PLUS = 5;
		public static int MINUS = 6;
		public static int STAR = 7;
		public static int FSLASH = 8;
		public static int PERCENT = 9;
		public static int CARET = 10;
		
		public static Lexer lexer;

		@Test
		public void test()
			{
				String input[] = {"(" ,  ")" , "012345678910" , "0123456789.98765432101" ,  "+" , "-" , "*" , "/" , "%" , "^" };
				String lexerInput = "()012345678910+-*/%^";

				Token tmp = null;
				int type = 2;
				int index = 0;
				
				lexer = new ExprLexer(lexerInput);
				Token lexerToken = null;
				
				for(int i=1;i<=9;i++)
					{
						tmp = new Token(type, input[index]);
						lexerToken = lexer.nextToken();
						
						assertEquals(tmp.type, lexerToken.type);
						
						type++;
						index++;
					}
				
				lexerToken = lexer.nextToken();
				assertEquals(lexerToken.type, Lexer.EOF_TYPE);
				
			}

	}
