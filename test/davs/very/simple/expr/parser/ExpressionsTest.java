package davs.very.simple.expr.parser;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExpressionsTest
	{

		@Test
		public void test()
			{
				String input1 = "(10+20+20+45+5)";
				
				Lexer lexer = new ExprLexer(input1);
				ExprParser parser = new ExprParser(lexer);
				
				assertEquals(100.00, parser.expression(), 0.11);
				
				
				
				String input2 = "5*5*10";
				
				lexer = new ExprLexer(input2);
				parser = new ExprParser(lexer);
				
				assertEquals(250.00, parser.expression(), 0.11);
				
				
				
				String input3 = "(12/2*5-55^2%5+1-2-0 + (45-5) ) * 10";
				
				lexer = new ExprLexer(input3);
				parser = new ExprParser(lexer);
				
				assertEquals(690.00, parser.expression(), 0.11);
				
				
				
				String input4 = "5 ^ (-2)";
				
				lexer = new ExprLexer(input4);
				parser = new ExprParser(lexer);
				
				assertEquals(0.04, parser.expression(), 0.11);
				
				
				
				String input5 = "(55*0) - (23/3) - 55 - (-5-5)";
				
				lexer = new ExprLexer(input5);
				parser = new ExprParser(lexer);
				
				assertEquals(-52.666666666666664, parser.expression(), 0.2);
				
				
				
				String input6 = "-100-100-100";
				
				lexer = new ExprLexer(input6);
				parser = new ExprParser(lexer);
				
				assertEquals(-300.00, parser.expression(), 0.11);
				
				
				
				String input7 = "-100*0*0*0 - 0.123345 - 0.35789 + 0.751 + 1.98752 + 2.253010";
				
				lexer = new ExprLexer(input7);
				parser = new ExprParser(lexer);
				
				assertEquals(4.510295, parser.expression(), 0.11);
				
				
				
				String input8 = "-0.5 + 0.5";
				
				lexer = new ExprLexer(input8);
				parser = new ExprParser(lexer);
				
				assertEquals(0.00, parser.expression(), 0.11);
				
			}

	}
