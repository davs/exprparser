package davs.very.simple.expr.parser;

import static org.junit.Assert.*;

import org.junit.Test;

public class ParserTest
	{

		@Test
		public void test()
			{
				String input = " ( (1+2-3)/4*(5%6^7) - (55 -98) ) ";
				boolean isError = false;
				
				Lexer lexer = new ExprLexer(input);
				ExprParser parser = new ExprParser(lexer);
				
				try
					{
						parser.expression();
						
					} catch (Exception e)
					{
						isError = true;
					}
				catch(Error e)
					{
						isError = true;
					}
				
				assertFalse(isError);
				
				String invalidInput = "+-/*44-+/3((";
				String invalidInput2 = "()";
				String invalidInput3 = "(7+25";
				String invalidInput4 = "  ";
				
				lexer = new ExprLexer(invalidInput);
				parser = new ExprParser(lexer);
				
				
				try
					{
						parser.expression();
						
					} catch (Exception e)
					{
						isError = true;
					}
				catch(Error e)
					{
						isError = true;
					}
			
				assertTrue(isError);
				
				lexer = new ExprLexer(invalidInput2);
				parser = new ExprParser(lexer);
				
				try
					{
						parser.expression();
						
					}  catch (Exception e)
						{
							isError = true;
						}
					catch(Error e)
						{
							isError = true;
						}
				
				assertTrue(isError);
			
			
			
				lexer = new ExprLexer(invalidInput3);
				parser = new ExprParser(lexer);
				
				try
					{
						parser.expression();
						
					}  catch (Exception e)
						{
							isError = true;
						}
					catch(Error e)
						{
							isError = true;
						}
				
				assertTrue(isError);
				
				
				lexer = new ExprLexer(invalidInput4);
				parser = new ExprParser(lexer);
				
				try
					{
						parser.expression();
						
					}  catch (Exception e)
						{
							isError = true;
						}
					catch(Error e)
						{
							isError = true;
						}
				
				assertTrue(isError);
			
			}

	}
