package davs.very.simple.expr.parser;

public class Token
	{
		int type;
		String text;

		public Token(int type, String text)
			{
				this.type = type;
				this.text = text;
			}
		public Token(int type)
			{
				this.type = type;
			}

		public String toString()
			{
				String tname = ExprLexer.tokenNames[type];
				return "<'" + text + "'," + tname + ">";
			}
		
		public String getText()
			{
				return text;
			}
	}
