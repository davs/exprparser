package davs.very.simple.expr.parser;

public class ExprParser extends Parser
 	{
		public ExprParser(Lexer input)
		{
			super(input);
		}
		
		/**
		 * Rule: 
		 * expression : multiply ( (+ | -) multiply )*
		 * 
		 * @return
		 */
		double expression()
			{
				
				double returnValue; // Return value and left side operand
				double rs; // Right side operand
				
				returnValue = multiply();
				
				while(lookahead.type == ExprLexer.PLUS || lookahead.type == ExprLexer.MINUS)
					{
						if(lookahead.type == ExprLexer.PLUS)
							{
								match(ExprLexer.PLUS);
								rs = multiply();
								returnValue = returnValue + rs;
							}
						else if(lookahead.type == ExprLexer.MINUS)
							{
								match(ExprLexer.MINUS);
								rs = multiply();
								returnValue = returnValue - rs;
							}
						else throw new Error("Expecting plus or minus; found " + lookahead);
						
					}
				
				return returnValue;
					
			}
		
		
		/**
		 * Rule:
		 * multiply : unary ((* | / | %) unary)*
		 * 
		 * @return
		 */
		double multiply()
			{
				double returnValue = unary();
				double rs;
				
				while(lookahead.type == ExprLexer.STAR || lookahead.type == ExprLexer.FSLASH || lookahead.type == ExprLexer.PERCENT)
					{
						if(lookahead.type == ExprLexer.STAR)
							{
								match(ExprLexer.STAR);
								rs = unary();
								returnValue = returnValue * rs;
							}
						else if(lookahead.type == ExprLexer.FSLASH)
							{
								match(ExprLexer.FSLASH);
								rs = unary();
								
								if(rs==0)
									{
										throw new Error("Division by zero");
									}
								returnValue = returnValue / rs;
							}
						else if(lookahead.type == ExprLexer.PERCENT)
							{
								match(ExprLexer.PERCENT);
								rs = unary();
								if(rs==0)
									{
										throw new Error("Division by zero");
									}
								
								returnValue = returnValue % rs;
							}
						else throw new Error("expecting star,fslash or percent; found "+lookahead);
					}
				
				return returnValue;
			}
		
		
		
		/**
		 * Rule:
		 * unary : (-)? exponent
		 * 
		 * @return
		 */
		double unary()
			{
				if(lookahead.type == ExprLexer.MINUS)
					{
						match(ExprLexer.MINUS);
						double returnValue = exponent();
						return returnValue * (-1);
					}
				else return	exponent();
			}
		
		
		
		/**
		 * Rule:
		 * exponent : atom (^ atom)*
		 * 
		 * @return
		 */
		double exponent()
			{
				double returnValue = atom();
				
				while(lookahead.type == ExprLexer.CARET)
					{
						match(ExprLexer.CARET);
						double rs = atom();
						
						returnValue = Math.pow(returnValue, rs);
					}
				
				return returnValue;
				
			}
		
		/**
		 * Rule:
		 * atom : number | '(' expression ')'
		 * 
		 * @return
		 */
		double atom()
		{
			double returnValue=0;
			
			if(lookahead.type == ExprLexer.NUMBER)
				{
					try
						{
							returnValue = Double.parseDouble(lookahead.getText());
							match(ExprLexer.NUMBER);
							return returnValue;
						} catch (NumberFormatException e)
						{
							System.err.println("Not a number: "+ lookahead);
						}
					
				}
			else if(lookahead.type == ExprLexer.LPAREN)
				{
					match(ExprLexer.LPAREN);
					returnValue = expression();
					
					try
						{
							match(ExprLexer.RPAREN);
						} catch (Exception e)
						{
							System.err.println("Unbalanced parens");
						}
					
					return returnValue;
					
				}
			else throw new Error("expecting number or expression; found "+lookahead);	
			
			return returnValue;
		}
		
	}
