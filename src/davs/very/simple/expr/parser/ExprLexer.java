package davs.very.simple.expr.parser;

public class ExprLexer extends Lexer
	{
		public static int LPAREN = 2;
		public static int RPAREN = 3;
		public static int NUMBER = 4;
		public static int PLUS = 5;
		public static int MINUS = 6;
		public static int STAR = 7;
		public static int FSLASH = 8;
		public static int PERCENT = 9;
		public static int CARET = 10;
		
		public static String[] tokenNames =
			{ "n/a", "<EOF>", "LPAREN", "RPAREN" , "NUMBER" , "PLUS" , "MINUS" , "STAR" , "FSLASH" , "PERCENT" , "CARET"};

		public String getTokenName(int x)
			{
				return tokenNames[x];
			}

		public ExprLexer(String input)
			{
				super(input);
			}

		boolean isNUMBER()
			{
				return (c >= '0' && c <= '9') || c=='.';
			}

		 // Based on char detect and create Token right Token type.
		public Token nextToken()
			{
				while (c != EOF)
					{
						switch (c)
							{
							case ' ':
							case '\t':
							case '\n':
							case '\r':
								WS();
								continue;
							case '(':
								consume();
								return new Token(LPAREN, "(");
							case ')':
								consume();
								return new Token(RPAREN, ")");
							case '+':
								consume();
								return new Token(PLUS, "+");
							case '-':
								consume();
								return new Token(MINUS, "-");
							case '*':
								consume();
								return new Token(STAR, "*");
							case '/':
								consume();
								return new Token(FSLASH, "/");
							case '%':
								consume();
								return new Token(PERCENT, "%");
							case '^':
								consume();
								return new Token(CARET, "^");
							default:
								if (isNUMBER())
									return NUMBER();
								throw new Error("invalid character: " + c);
							}
					}
				return new Token(EOF_TYPE, "<EOF>");
			}
		
		
		//NUMBER: (0-9)+ or '.' for double types
		Token NUMBER()
			{
				StringBuilder buf = new StringBuilder();
				do
					{
						buf.append(c);
						consume();
					} while (isNUMBER() || c=='.');
				return new Token(NUMBER, buf.toString());
			}

		// Ignore whitespace
		void WS()
			{
				while (c == ' ' || c == '\t' || c == '\n' || c == '\r')
					consume();
			}
	}
