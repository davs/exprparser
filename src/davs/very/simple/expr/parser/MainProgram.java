package davs.very.simple.expr.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainProgram
	{
		
		public static void main(String[] args) throws IOException
			{
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
				String input;
				
				while(true)
					{
						System.out.println("Enter new expression or enter \'q\' to exit  : ");
						input = br.readLine();
						
						if(input.equals("q"))
							break;
						
						ExprLexer lexer = new ExprLexer(input);
						ExprParser parser =  new ExprParser(lexer);
						
						System.out.println("\n\n\n\n\n\n\n");
						System.out.println("Evaluated to : " + parser.expression() ); 
					}
				
			}

	}
