Small (numeric) expressions parser, built by hand in Java. Reason for building such parser
is to get better idea how and why ANTLR generates lexers and parsers. Supported operations
and language specification can be found in langDef file. To start program navigate to
SimpleExprParser.jar (found in bin folder) and type following in the terminal: java -jar SimpleExprParser.jar
